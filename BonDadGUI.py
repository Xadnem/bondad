import pymysql
import os
import errno

from tkinter import *
from tkinter import ttk

from sys import exit,executable
from os import execl

import BonDad
from BonDad import Bondad

class BonDadGUI():
    def __init__(self):
        # Atributos
        self.HOST = ""
        self.USER = ""
        self.PASS = ""
        self.NAME = ""
        # Inicializar la ventana principal
        self.root = Tk()
        self.root.title("BonDad Xadnem 20")
        self.root.configure(background="seagreen")  # Cambiar el color del fondo
        self.alto, self.ancho = self.root.winfo_screenheight(), self.root.winfo_screenwidth()  # Obtener el ancho y alto de la pantalla
        self.root.geometry("%dx%d+0+0" % (self.ancho, self.alto))  # Cambiar el tamaño de la ventana
        # Etiqueta para los mensajes
        self.Mensaje = StringVar()
        self.Mensaje.set("Introducir los parámetros para la conexión con la base de datos.")
        self.lbMensajes = Label(self.root, textvariable=self.Mensaje, bg="darkseagreen", fg="Black",
                                justify="left", anchor=W, font=("Dejavu", 23, "bold"), wraplength=self.ancho - 150, pady="0")
        self.lbMensajes.place(x=5, y=0, height=self.alto / 7, width=self.ancho - 150)
        # Botones de navegacion
        self.btSalir = Button(self.root, text="Salir", background="red",
                              command=self.on_btSalir_clicked, fg="orange", activebackground="Yellow")
        self.btSalir.place(x=self.ancho - 120, y=10, width=70, height=30)

        self.btNuevo = Button(self.root, text="Nuevo", background="silver", activebackground="Yellow", fg="Black",
                              command=self.on_btNuevo_clicked)
        self.btNuevo.place(x=self.ancho - 120, y=60, width=70, height=30)
        # Etiqueta para el rotulo del nombre del host
        rotulos = ("Dejavu",17)
        self.lbHost = Label(self.root,text = "Nombre del host: ",bg = "seagreen",fg="Black",justify = "left",
                            font = rotulos,anchor = "w")
        self.lbHost.place(x = 10,y = self.alto/6.5,width = self.ancho/9)
        # TextBox para el nombre del host
        fondocajas = "White"
        letracajas = ("Dejavu",17)
        self.tbHost = Entry(self.root,bg = fondocajas,font = letracajas,fg = "Black")
        self.tbHost.place(x = self.ancho/8,y = self.alto/6.5,width = self.ancho/1.5)
        self.tbHost.focus_set()
        self.tbHost.bind('<Return>', self.on_tbHost_activate)

        # Etiqueta para el rotulo del nombre del usuario
        self.lbUser = Label(self.root, text="Nombre del usuario: ", bg="seagreen", fg="Black", justify="left",
                            font=rotulos, anchor="w")
        self.lbUser.place(x=10, y=self.alto / 5.2, width=self.ancho / 8)
        # TextBox para el nombre del usuario
        self.tbUser = Entry(self.root, bg=fondocajas, font=letracajas, fg="Black")
        self.tbUser.place(x=self.ancho / 8, y=self.alto / 5.2, width=self.ancho / 1.5)
        self.tbUser.bind('<Return>', self.on_tbUser_activate)

        # Etiqueta para el rotulo del password
        self.lbPw = Label(self.root, text="Password: ", bg="seagreen", fg="Black", justify="left",
                            font=rotulos, anchor="w")
        self.lbPw.place(x=10, y=self.alto / 4.32, width=self.ancho / 8)
        # TextBox para el password
        self.tbPw = Entry(self.root, bg=fondocajas, font=letracajas, fg="Black")
        self.tbPw.place(x=self.ancho / 8, y=self.alto / 4.32, width=self.ancho / 1.5)
        self.tbPw.bind('<Return>', self.on_tbPw_activate)

        # Etiqueta para el rotulo del nombre de la base de datos
        self.lbDb = Label(self.root, text="Base de datos: ", bg="seagreen", fg="Black", justify="left",
                          font=rotulos, anchor="w")
        self.lbDb.place(x=10, y=self.alto / 3.7, width=self.ancho / 8)
        # TextBox para el nombre de la base de datos
        self.tbDb = Entry(self.root, bg=fondocajas, font=letracajas, fg="Black")
        self.tbDb.place(x=self.ancho / 8, y=self.alto / 3.7, width=self.ancho / 1.5)
        self.tbDb.bind('<Return>', self.on_tbDb_activate)

        self.root.mainloop()


    def on_btSalir_clicked(self):
        """ Manejador para el boton Salir """

        sys.exit()


    def on_btNuevo_clicked(self):

        """ Manejador para el boton Nuevo, que reinicia el programa """

        python = sys.executable
        os.execl(python, python, *sys.argv)

    def on_tbHost_activate(self,widget):
        """ Manejador del evento activate de la caja de texto para introducir el nombre del host.
                   asigna el nombre introducido al atribyto host de la clase."""
        ps = self.tbHost.get()
        if len(ps) == 0:
            return
        self.HOST = ps
        self.tbUser.focus_set()

    def on_tbUser_activate(self,widget):
        """ Manejador del evento activate de la caja de texto para introducir el nombre del usuario.
                   asigna el nombre introducido al atribyto user de la clase."""
        ps = self.tbUser.get()
        if len(ps) == 0:
            return
        self.USER = ps
        self.tbPw.focus_set()


    def on_tbPw_activate(self, widget):
        """ Manejador del evento activate de la caja de texto para introducir el password.
               asigna el nombre introducido al atribyto passw de la clase."""
        ps = self.tbPw.get()
        #if len(ps) == 0:
            #return
        self.PASS = ps
        self.tbDb.focus_set()

    def on_tbDb_activate(self, widget):
        """ Manejador del evento activate de la caja de texto para introducir el nombre de la base de datos.
               asigna el nombre introducido al atribyto name de la clase."""
        ps = self.tbDb.get()
        if len(ps) == 0:
            return
        self.NAME = ps
        self.CrearConexion()
        #bondad = Bondad(self.host,self.user,self.passw,self.name)
        #self.tbPw.focus_set()

    def CrearConexion(self):
        """ Intenta realizar la conexion con los datos aportados y si no es posible, muestra el
            mensaje de error correspondiente en la etiqueta de los mensajes """
        # Meter los datos en una lista
        datos = datos = [self.HOST, self.USER, self.PASS, self.NAME]
        try:
            # Intentar crear la conexion
            self.conexion = pymysql.connect(*datos)  # Conectar a la base de datos
            self.Mensaje.set("Conexión establecida en el host: %s Usuario: %s Password: xxxxxx, base de datos: %s."
                             "\nSeleccionar las tablas sobre las que se realizaran operaciones. " %(self.HOST,self.USER,self.NAME))
            self.lbHost.destroy()
            self.tbHost.destroy()
            self.lbUser.destroy()
            self.tbUser.destroy()
            self.lbPw.destroy()
            self.tbPw.destroy()
            self.lbDb.destroy()
            self.tbDb.destroy()
            bondad = Bondad(self.conexion,self.root,self.ancho,self.alto,self.Mensaje)
        except(pymysql.err.OperationalError):
            e = str(sys.exc_info()[1]) # Mensaje de error de la excepcion
            numeroerror = int(e[1:5])
            if(numeroerror == 2003):
                self.Mensaje.set("Nombre de host desconocido")
                self.tbHost.focus_set()
            elif(numeroerror == 1049):
                self.Mensaje.set("Nombre de base de datos desconocida")
                self.tbDb.focus_set()




gui = BonDadGUI()


