import pymysql
import sys

import tkinter
from tkinter import *

class Bondad():
    def __init__(self,conexion,root,ancho,alto,Mensaje):
        self.ancho = ancho
        self.alto = alto
        self.tablas = []
        self.Mensaje = Mensaje
        # Crear un objeto cursor con la conexion pasada como argumento
        self.cursor = conexion.cursor()
        # Rotulo de la lista de tablas
        self.lbTablas = Label(root,text = "Tablas disponibles",font = ("Dejavu",17),underline = 1,bg = "seagreen")
        self.lbTablas.place(x = 20, y = self.alto/6)
        # Mostrar las tablas contenidas en la base de datos a la que se ha conectado
        self.cursor.execute("show tables")
        tables = [ t for t in self.cursor]
        self.listatablas = Listbox(root,font = ("Dejavu",17),selectmode = "MULTIPLE",selectbackground = "white")
        self.listatablas.place(x = 10, y = self.alto/5.1 )
        self.listatablas.bind("<<ListboxSelect>>", self.onSelect)
        for s in tables:
            self.listatablas.insert(END,s,)
        # Rotulo del ListBox donde se mostraran las tablas seleccionadas
        self.lbrotuloseleccionadas = Label(root,text = "Tablas seleccionadas: ",font = ("Dejavu",17),underline = 1,bg = "seagreen")
        self.lbrotuloseleccionadas.place(x = ancho/3.5, y = self.alto/6)
        # ListBox de las tablas seleccionadas
        self.listatablasselec = Listbox(root,font = ("Dejavu",17),selectbackground = "coral")
        self.listatablasselec.place(x = ancho/3.5, y = self.alto/5.1 )
        self.listatablasselec.bind("<<ListboxSelect>>", self.on_Seleccionadas_Select)
        # Botón para terminar la seleccion
        self.btTermSel = Button(root, text="Finalizar selección", background="chartreuse",
                              command=self.on_btTermSel_clicked, fg="Green", activebackground="Yellow",
                                font = ("Dejavu",13,"bold"),wraplength = 90)
        self.btTermSel.place(x = ancho/3.5,y = self.alto/2)
        # Rotulo del ListBox donde se mostraran los campos cuando se seleccione una de las tablas seleccionadas
        self.lbrotulocampos = Label(root, text="Campos: ", font=("Dejavu", 17), underline=1,
                                           bg="seagreen")
        # ListBox para mostrar los atributos de las tablas que se seleccionen en el listbox de las tablas seleccionadas
        self.listacampos = Listbox(root, font=("Dejavu", 17), selectbackground="coral")
        self.listacampos.bind("<<ListboxSelect>>", self.on_listacampos_select)

        # print(str(conexion))
        """
        self.HOST = HOST
        self.USER = USER
        self.PASS = PASS
        self.NAME = NAME
        datos = [self.HOST, self.USER, self.PASS, self.NAME]
        try:
            self.conexion = pymysql.connect(*datos)  # Conectar a la base de datos

            print(str(self.conexion))
        except(pymysql.err.OperationalError):
            e = sys.exc_info()[1]
            print(e)
        """
        """
        dato = input("Dato: ")
        id, titulo, autor, ano = dato.split(',')
        id = int(id)
        ano = int(ano)
        query = "INSERT INTO libros VALUES (%d , '%s', '%s',%d)" % (id, titulo, autor, ano)
        self.run_query(query)
        """



    def run_query(self, query=''):
        datos = [self.HOST, self.USER, self.PASS, self.NAME]
        conn = pymysql.connect(*datos)  # Conectar a la base de datos
        cursor = conn.cursor()  # Crear un cursor
        cursor.execute(query)  # Ejecutar una consulta

        if query.upper().startswith('SELECT'):
            data = cursor.fetchall()  # Traer los resultados de un select
        else:
            conn.commit()  # Hacer efectiva la escritura de datos
            data = None

        print(data)

        cursor.close()  # Cerrar el cursor
        conn.close()  # Cerrar la conexión

        return data

    def onSelect(self,val):
        """ Mete las tablas seleccionadas en el ListBox de las tablas disponibles en el ListBox de
            las tablas seleccionadas. Si se pulsa sobre un elemento ya seleccionado, se deselecciona y
            se borra del ListBox de las tablas seleccionadas"""
        sender = val.widget # obtener el listbox widget que se ha pulsado
        idx = sender.curselection() # obtener el indice del widget de la lista que se ha pulsado
        value = sender.get(idx) # Obtener el contenido del widget pulsado
        if value in self.tablas: # Deseleccionar la tabla si estaba seleccionada
            # Buscar el indice de la tabla deseleccionado  en el ListBox de tablas seleccionadas
            indisec = self.IndiceEnSeleccionadas(value[0])
            self.listatablasselec.delete(indisec)
            # Despintar las tablas deseleccionadas en el ListBox de tablas disponibles
            self.listatablas.itemconfig(idx, {'fg': 'black','bg':'white'})
            # Eliminar la tabla deseleccionada del ListBox de tablas seleccionadas
            self.tablas.remove(value)
        else:
             self.listatablas.itemconfig(idx, {'fg': 'red', 'bg': 'coral'})  # Pintar de rojo las tablas seleccionadas
             self.tablas.append(value)
             self.listatablasselec.insert(idx,value)
        # print(self.tablas)

    def IndiceEnSeleccionadas(self,entrada):
        """ Devuelve el indice del elemento pasado como argumento en la ListBox de las tablas seleccionadas.
            Si el elemento no existe, devuelve -1"""

        for i in range(len(self.tablas)):
            lectura = (self.listatablasselec.get(i))[0]
            if lectura == entrada:
                return i
        return -1

    def on_btTermSel_clicked(self):
        """ Termina la selección de tablas y muestra las tablas seleccionadas """
        self.MostrarSeleccionadas()

    def on_Seleccionadas_Select(self,val):
        """ Muestra los campos que contienen las tablas seleccionadas en el paso anterior
            cuando se hace click sobre ellas y permite añadir condiciones de busqueda sobre
            los campos que contienen """
        # Borrar el posible contenido del listbox listacampos
        self.listacampos.delete(0,END)

        sender = val.widget  # obtener el listbox widget que se ha pulsado
        idx = sender.curselection()  # obtener el indice del widget de la lista que se ha pulsado
        tabla = sender.get(idx)  # Obtener el contenido del widget pulsado ( una de las tablas seleccionadas en el paso anterior )

        # print(tabla)
        # campostablas = []

        #for t in self.tablas:
        listacampos = []
        tabla = (str(tabla)) # Convertir el nombre de la tabla en un string y eliminar las comillas
        tabla = tabla[2:len(tabla)-3]
        # print(tabla)
        # Mostrar la descripción de la tabla y meterla en una lista
        self.cursor.execute("show create table " + tabla)
        camposunidos = [c for c in self.cursor]
        # Descomponer la descripcion de la tabla y meter cada campo en un elemento de una lista
        campos = str(camposunidos[0]).split(',')
        campos.pop(0) # Eliminar el primer registro que solo contiene el nombre de la tabla
        # Limpiar el contenido del segundo registro
        indice = campos[0].index('(')
        campos[0] = campos[0][indice+1:]
        # Limpiar el contenido del ultimo registro
        indice = campos[len(campos)-1].index("ENGINE")
        campos[len(campos)-1] = campos[len(campos)-1]
        campos[len(campos)-1] = campos[len(campos)-1][:indice]
        # print(campos)
        if "FOREIGN KEY" in campos[len(campos)-1]:
            indice = campos[len(campos)-1].index("FOREIGN KEY")
            campos[len(campos)-1] = campos[len(campos)-1][indice:len(campos[len(campos)-1])-2]
        for i,campo in enumerate(campos):
            # Eliminar los registros inutiles
            if "UNIQUE KEY" in campo:
                campos.pop(i)
            else:
                # print(campos[i])
                # Si se trata de un campo de tipo decimal concatenarle el campo siguiente,
                # y eliminarlo, ya que el tipo decimal contiene una coma
                if "decimal" in campos[i]:
                    campos[i] = campos[i] + ','+ campos[i+1]
                    campos.pop(i+1)

                # Si se trata de una primary key compuesta, concatenar el campo siguiente y
                # eliminarlo, ya que las primary key compuestas tienen una coma
                elif "PRIMARY KEY" in campos[i] and campos[i][len(campos[i])-1] != ')':
                    try:
                        campos[i] = campos[i] + ',' + campos[i + 1]
                        campos.pop(i + 1)
                    except IndexError:
                        pass
                listacampos.append(campos[i])
        # Insertar y mostrar los campos de la tabla seleccionada en el listbox listacampos
        anchorequerido = 0
        for x in listacampos:
            x = x.replace("\\n","")
            x = x.strip()
            self.listacampos.insert(END,x)
            if len(x) > anchorequerido:
                anchorequerido = len(x)
            # print(x)
        # Mostrar el listbox con los campos de la tabla seleccionada
        self.lbrotulocampos.place(x=self.ancho / 2.5, y=self.alto / 6)
        self.listacampos.place(x=self.ancho / 2.5, y=self.alto / 5.1, width = anchorequerido * 13,height = len(listacampos)*37)


    def on_listacampos_select(self):
        pass


    def MostrarSeleccionadas(self):
        self.listatablas.destroy()
        self.lbTablas.destroy()
        self.btTermSel.destroy()
        self.lbrotuloseleccionadas.place(x = 10, y = self.alto/6)
        self.listatablasselec.place(x = 10, y = self.alto/5.1)
        self.Mensaje.set("Seleccionar los campos y las condiciones de busqueda en la base de datos.")


        


# Hay que introducir los valores separados por comas. ie: 4,Seven Eves,Neal Stephenson,1980

# base = Bondad("localhost","Xadnem","","PruebaPython")


